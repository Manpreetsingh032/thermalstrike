package `in`.his.thermalstrike

import `in`.his.thermalstrike.database.DeviceListTable
import `in`.his.thermalstrike.database.DeviceListTableDao
import `in`.his.thermalstrike.database.NameTempTable
import `in`.his.thermalstrike.database.NameTempTableDao
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [DeviceListTable::class,NameTempTable::class],
    version = 1,
    exportSchema = false
)
abstract class ParameterRoomDatabase : RoomDatabase() {

    abstract fun deviceListTableDao(): DeviceListTableDao
    abstract fun nameTempTableDao(): NameTempTableDao

    companion object {
        @Volatile
        private var INSTANCE: ParameterRoomDatabase? = null

        fun getDatabase(context: Context): ParameterRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ParameterRoomDatabase::class.java,
                    "ParameterDatabase"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }


}