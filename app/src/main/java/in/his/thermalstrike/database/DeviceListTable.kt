package `in`.his.thermalstrike.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "DeviceListTable")
class DeviceListTable(

    @PrimaryKey(autoGenerate = true)
    val deviceId: Int,

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "address")
    val address: String?,

    @ColumnInfo(name = "strength")
    val strength: String?,

    @ColumnInfo(name = "battery")
    val battery: String?,

    @ColumnInfo(name = "temperature")
    val temperature: String?

)
