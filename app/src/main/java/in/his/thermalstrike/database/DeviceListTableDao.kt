package `in`.his.thermalstrike.database

import androidx.room.*

@Dao
interface DeviceListTableDao {

    /**
     * Get a user by id.
     * @return the user from the table with a specific id.
     */
    @Query("SELECT * FROM DeviceListTable WHERE deviceId = :id")
    fun getParameterById(id: String): List<DeviceListTable>

    /*   @Query("SELECT * FROM DeviceListTable WHERE address = :name and strength = :rollNo")
       fun getChildByNameRollNo(name: String, rollNo: String): List<DeviceListTable>*/

    @Query("SELECT * from DeviceListTable")
    fun getAllData(): List<DeviceListTable>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(device: DeviceListTable)

    @Query("DELETE FROM DeviceListTable")
    fun deleteAll()

    @Delete
    fun delete(deviceListTable: DeviceListTable)

    @Update
    fun update(deviceListTable: DeviceListTable)

}