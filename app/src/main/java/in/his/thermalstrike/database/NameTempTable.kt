package `in`.his.thermalstrike.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "NameTempTable")
class NameTempTable(

    @PrimaryKey(autoGenerate = true)
    val nameTempId: Int,

    @ColumnInfo(name = "name")
    val name: String?,

    @ColumnInfo(name = "address")
    val address: String?,
)