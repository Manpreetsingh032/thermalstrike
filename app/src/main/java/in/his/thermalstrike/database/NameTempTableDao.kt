package `in`.his.thermalstrike.database

import androidx.room.*

@Dao
interface NameTempTableDao {

    @Query("SELECT * FROM NameTempTable WHERE nameTempId = :id")
    fun getParameterById(id: String): List<NameTempTable>

    @Query("SELECT * from NameTempTable")
    fun getAllData(): List<NameTempTable>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(device: NameTempTable)

    @Delete
    fun delete(nameTempTable: NameTempTable)

    @Update
    fun update(nameTempTable: NameTempTable)
}