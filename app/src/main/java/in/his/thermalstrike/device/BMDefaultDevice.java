package in.his.thermalstrike.device;

import android.bluetooth.BluetoothDevice;
import android.view.ViewGroup;

/**
 * Created by Garrett on 25/08/2016.
 */
public class BMDefaultDevice extends BMDevice {

    /**
     * Constructor
     *
     * @param device  Bluetooth device
     * @param version Version of this Blue Maestro device
     */
    public BMDefaultDevice(BluetoothDevice device, byte version) {
        super(device, version);
    }
/*
    @Override
    public void setupChart(Chart chart, String command) {

    }*/

    public void updateViewGroupForDetails(ViewGroup vg) {

    }
   /* @Override
    public void updateChart(Chart chart, String text) {

    }*/
}
