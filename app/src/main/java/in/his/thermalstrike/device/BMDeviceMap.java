package in.his.thermalstrike.device;

import android.bluetooth.BluetoothDevice;
import android.content.Context;


import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Garrett on 15/08/2016.
 *
 * This is a map which will create a Blue Maestro device, given its ID.
 * This allows less duplication of code, and contains the Blue Maestro device
 * creation in one area
 */
public enum BMDeviceMap {

    INSTANCE;

    /**
     * Map of ID to Blue Maestro device
     */
    private final Map<Byte, Class<? extends BMDevice>> dvMap;
    private final Map<String, BMDevice> deviceMap;

    /**
     * IDs
     */
    private static final byte TEMPO_DISC_T          = 1;
    private static final byte TEMPO_TEMP            = 13;
    private static final byte TEMPO_TEMP_BEACON     = 113;
    private static final byte TEMPO_TEMPHUMI22      = 22;
    private static final byte TEMPO_TEMPHUMI23      = 23;
    private static final byte PEBBLE27              = 27;
    private static final byte BAIT_SAFE             = 33;
    private static final byte MOVE_LOGGER           = 32;
    private static final byte BM_BUTTON             = 42;

    /**
     * Constructor
     */
    BMDeviceMap(){
        this.dvMap = new HashMap<Byte, Class<? extends BMDevice>>();
        this.deviceMap = new HashMap<String, BMDevice>();

        dvMap.clear();
        deviceMap.clear();

        initializeIDs();
    }

    /**
     * Initialize IDs to classes
     */
    public final void initializeIDs(){
        // Blue Maestro devices
//        dvMap.put(TEMPO_DISC_T,             BMTempoDiscT.class);
        dvMap.put(TEMPO_TEMP,               BMTemp13.class);
       /* dvMap.put(TEMPO_TEMP_BEACON,        BMTemp13.class);
        dvMap.put(TEMPO_TEMPHUMI22,         BMTempHumi22.class);
        dvMap.put(TEMPO_TEMPHUMI23,         BMTempHumi23.class);
        dvMap.put(PEBBLE27,                 BMPebble27.class);
        dvMap.put(BAIT_SAFE,                BMBaitsafe.class);
        dvMap.put(MOVE_LOGGER,              BMMoveLogger32.class);
        dvMap.put(BM_BUTTON,                BMButton42.class);*/

    }

    public final void clear(){

    }

    /**
     * Creates a Blue Maestro device. If not found, will default to a generic Blue Maestro device.
     * @param id The ID of the Blue Maestro device
     * @param device The BluetoothDevice currently viewed
     * @return The Blue Maestro device
     */
    public final BMDevice createBMDevice(byte id, BluetoothDevice device){
        BMDevice bmDevice = null;
        Class<? extends BMDevice> clazz = dvMap.get(id);
        if(clazz != null){
            try {
                bmDevice = clazz
                            .getDeclaredConstructor(BluetoothDevice.class, byte.class)
                            .newInstance(device, id);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        if(bmDevice == null) bmDevice = new BMDefaultDevice(device, (byte) 0xFF);
        deviceMap.put(device.getAddress(), bmDevice);
        return bmDevice;
    }
    /**
     * Gets a current Blue Maestro device
     * @param address The address of the device
     * @return The Blue Maestro device
     */
    public final BMDevice getBMDevice(String address){
        return deviceMap.get(address);
    }


}
