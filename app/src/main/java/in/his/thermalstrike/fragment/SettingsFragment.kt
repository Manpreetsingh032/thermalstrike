package `in`.his.thermalstrike.fragment

import `in`.his.thermalstrike.HandleSharedPreference
import `in`.his.thermalstrike.R
import `in`.his.thermalstrike.adapter.SettingsBedBugAdapter
import `in`.his.thermalstrike.databinding.FragmentSettingsBinding
import `in`.his.thermalstrike.models.BedBugModel
import `in`.his.thermalstrike.models.TempTimeModel
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {
    private var fragmentSettingsBinding: FragmentSettingsBinding? = null
    private val binding get() = fragmentSettingsBinding!!
    val bugList = ArrayList<BedBugModel>()
    private val bugTT = ArrayList<TempTimeModel>()
    private lateinit var c: Context
    lateinit var sharedPreference: HandleSharedPreference


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentSettingsBinding = FragmentSettingsBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPreference = HandleSharedPreference(c)
        settingsBedBugAdapter = SettingsBedBugAdapter(c)
        val linearLayoutManager =
            LinearLayoutManager(this.requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvBug.layoutManager = linearLayoutManager
        binding.rvBug.adapter = settingsBedBugAdapter

        binding.saveBug.setOnClickListener {
            sharedPreference.saveTemp(binding.tempBug.text.toString())
            sharedPreference.saveTime(binding.timeBug.text.toString())
            bugTT[6] =
                TempTimeModel(binding.tempBug.text.toString(), binding.timeBug.text.toString())

            if (switch1.isChecked) {
                sharedPreference.savePredictive(true)
            } else {
                sharedPreference.savePredictive(false)
            }
            if (switch2.isChecked) {
                sharedPreference.saveAlarm(true)
            } else {
                sharedPreference.saveAlarm(false)
            }
            if (switch3.isChecked) {
                sharedPreference.saveEmail(true)
            } else {
                sharedPreference.saveEmail(false)
            }

        }

        binding.rvBug.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val position: Int = linearLayoutManager.findFirstVisibleItemPosition()
                Log.d("TAG", "onScrollStateChanged: $position")
                var value1 = bugTT[position].bugTemp
                var value2 = bugTT[position].bugTime

                var imgDes = ArrayList<TempTimeModel>()

                imgDes.add(TempTimeModel(value1, value2))
                binding.tempBug.setText(value1)
                binding.timeBug.setText(value2)

                if (position == 6) {
                    binding.tempBug.isEnabled = true
                    binding.timeBug.isEnabled = true
                } else {
                    binding.tempBug.isEnabled = false
                    binding.timeBug.isEnabled = false
                }
            }
        })

        binding.switch1.isChecked = sharedPreference.getPredictiveValueSwitch()!!
        binding.switch2.isChecked = sharedPreference.getAlarmSwitch()!!
        binding.switch3.isChecked = sharedPreference.getEmailSwitch()!!


        categories()
        description()
    }

    private fun categories() {
        bugList.add(
            BedBugModel(
                R.drawable.covid,
                "COVID-19",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp. 158ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.bed_bug,
                "Bedbugs" + "",
                "Bed bugs are insects that feed on human blood, usually at night. Their bites can result in a health impacts including skin rashes,psychological effects, and allergy symptoms.",
                "Target Temp. 120ºF for 30 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.carpet_beetle,
                "Carpet Beetles",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp. 158ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.humanlice,
                "Lice",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp.122º-131ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.cloth_moth,
                "Cloth Moth",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp. 125ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.pantry_moths,
                "Pantry Moth",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp. 125ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.ic_websites__1_,
                "Custom Monitoring",
                "Manual Entry for Time and Temperature. ",
                "(Boxes are Editable)"
            )
        )

        SettingsBedBugAdapter.a.bugInfoData(bugList, bugTT)
    }

    private fun description() {
        bugTT.add(
            TempTimeModel("158", "60")
        )
        bugTT.add(
            TempTimeModel("120", "30")
        )
        bugTT.add(
            TempTimeModel("158", "60")
        )
        bugTT.add(
            TempTimeModel("122-131", "60")
        )
        bugTT.add(
            TempTimeModel("125", "60")
        )
        bugTT.add(
            TempTimeModel("125", "60")
        )
        bugTT.add(
            TempTimeModel(
                sharedPreference.getTemp().toString(),
                sharedPreference.getTime().toString()

            )
        )
    }


    override fun onDestroyView() {
        super.onDestroyView()
        fragmentSettingsBinding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        c = context
    }


    /* override fun bugTempTime(bugTemp: String, bugTime: String, position: Int) {
         binding.tempBug.setText(bugTemp)
         binding.timeBug.setText(bugTime)
         if (position == 6) {
             binding.tempBug.isEnabled = true
             binding.timeBug.isEnabled = true
         } else {
             binding.tempBug.isEnabled = false
             binding.timeBug.isEnabled = false
         }

     }*/

    companion object {
        lateinit var settingsBedBugAdapter: SettingsBedBugAdapter
        fun newInstance() =
            SettingsFragment()
    }

}