package `in`.his.thermalstrike.fragment

import `in`.his.thermalstrike.R
import `in`.his.thermalstrike.adapter.BedBugAdapter
import `in`.his.thermalstrike.adapter.BugInfoAdapter
import `in`.his.thermalstrike.databinding.FragmentBugInfoBinding
import `in`.his.thermalstrike.models.BedBugModel
import `in`.his.thermalstrike.models.BugInfoModel
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class BugInfoFragment : Fragment() {
    private var bugInfoBinding: FragmentBugInfoBinding? = null
    private val binding get() = bugInfoBinding!!
    val bugList = ArrayList<BedBugModel>()
    private val bugAbout = ArrayList<BugInfoModel>()
    lateinit var c: Context


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bugInfoBinding = FragmentBugInfoBinding.inflate(layoutInflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        bugInfoBinding = null
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bedBugAdapter = BedBugAdapter(c)
        val bugTitle =
            LinearLayoutManager(this.requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.bugItemRv.layoutManager = bugTitle
        binding.bugItemRv.adapter = bedBugAdapter

        bugInfoAdapter = BugInfoAdapter(c)
        val bugIdentity =
            LinearLayoutManager(this.requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.bugIdentificationRv.layoutManager = bugIdentity
        binding.bugIdentificationRv.adapter = bugInfoAdapter


        binding.bugItemRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val position: Int = bugTitle.findFirstVisibleItemPosition()
                Log.d("TAG", "onScrollStateChanged: $position")

                var value1 = bugAbout[position].bugInfoImg
                var value2 = bugAbout[position].bugInfoDes

                var imgDes = ArrayList<BugInfoModel>()

                imgDes.add(BugInfoModel(value1, value2))
                bugInfoAdapter.setList(imgDes)
                binding.bugIdentificationRv.layoutManager!!.scrollToPosition(position)
            }
        })

        categories()
        description()
        var value1 = bugAbout[0].bugInfoImg
        var value2 = bugAbout[0].bugInfoDes
        var imgDes = ArrayList<BugInfoModel>()
        imgDes.add(BugInfoModel(value1, value2))
        bugInfoAdapter.setList(imgDes)
    }


    private fun categories() {
        bugList.add(
            BedBugModel(
                R.drawable.covid,
                "COVID-19",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp. 158ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.bed_bug,
                "Bedbugs" + "",
                "Bed bugs are insects that feed on human blood, usually at night. Their bites can result in a health impacts including skin rashes,psychological effects, and allergy symptoms.",
                "Target Temp. 120ºF for 30 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.carpet_beetle,
                "Carpet Beetles",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp. 158ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.humanlice,
                "Lice",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp.122º-131ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.cloth_moth,
                "Cloth Moth",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp. 125ºF for 60 minutes"
            )
        )
        bugList.add(
            BedBugModel(
                R.drawable.pantry_moths,
                "Pantry Moth",
                "For Personal Protective Gear require 156ºF for 1 hour.",
                "Target Temp. 125ºF for 60 minutes"
            )
        )
        BedBugAdapter.a.bugData(bugList)
    }

    private fun description() {
        bugAbout.add(
            BugInfoModel(
                R.drawable.covid,
                "VIRAL AND PATHEGONS(Target temperature 138º-156ºF)\n" + "\n" + "2020 created many unique challenges worldwide.  When there were just 700 cases of COVID-19 in the U.S., the Ranger was already being deployed in nursing homes, hospitals, and intensive care units to decontaminate protective gear for the front line. \n" +
                        "\n" +
                        "ThermalStrike meets the FDA requirements for a Tier 3 Bioburden Reduction System."
            )
        )
        bugAbout.add(
            BugInfoModel(
                R.drawable.bed_bug, "BEDBUGS(Target temperature 120ºF)\n" +
                        "\n" +
                        "Bed bugs have conquered quite diverse locations, ranging from hospitals, hotels to trains, cruise ships and even airplanes. When dealing with an infestation at home or abroad, it is critical to limit any further spread. \n" +
                        "\n" +
                        "Most Commonly Spread By:\n" +
                        "\n" +
                        "Luggage and Travel Gear. Bedbugs typically stowaway in luggage or seams of backpacks, purses, etc.  Suspect luggage should be treated prior to unpacking.\n" + "\n" +
                        "Furnishings. Used couches, mattresses, bed frames, and night stands are all highly should generally be avoided.  Discarded items should be identified with a warning.\n" + "\n" +
                        "Adjoining Rooms and  Properties. Bedbugs can quickly spread between multi-family housing units through cracks and gaps around electrical outlets.\n" +
                        "\n" +
                        "Primary Items for Treatment\n" +
                        "\n" +
                        "Bedding:  Bedbugs  will typically stay close  to a sleeping area if there is a food source.  Blankets, pillows, and sheets should routinely be treated during an infestation to eliminate eggs and colonies.\n" + "\n" +
                        "Luggage:  Having a plan setting up the Ranger before traveling will be a welcome sight when you return.  Remove any heat sensitive items such as medicines, deodorants or cosmetics.  In most cases, bugs will not travel on a person.  \n" + "\n" +
                        "Heat Sensitive Items:  Never heat treat medicine – bottles should be inspected and wiped down.  Items such as computers should be treated separate"
            )
        )
        bugAbout.add(
            BugInfoModel(
                R.drawable.carpet_beetle,
                "CARPET BEETLES(Target temperature 120ºF)\n" + "\n" + "CARPET BEETLES DO NOT BIT HUMANS.  They feed on dead animal products, dander, and other debris.  The larvae, however, do chew through fabric causing damage.\n" + "\n" +
                        "Carpet beetles are often attracted to light and warmth indoors. Often, they’ll just fly inside your home, but can also get inside on pets or clothes.\n" + "\n" +
                        "If carpet beetles are living in your bed, it can be hard to tell if you have them or bed bugs. Both can live in mattresses and other bedding, and are attracted to the carbon dioxide you exhale as you sleep.\n" +
                        "\n" +
                        "How to Treat for Carpet Beetles\n" +
                        "\n" +
                        "Vacuum:  Vacuum floors, carpets, and heating vents – especially around the edges and corners.\n" + "\n" +
                        "Clothing and Fabrics:  Clothing and other fabrics are easily treated.\n" + "\n" +
                        "Bedding:  If carpet beetles are found near the bed, treat linens and bedding in a similar manner as bedbugs. A mattress encasement may be helpful.\n" +
                        "\n"
            )
        )
        bugAbout.add(
            BugInfoModel(
                R.drawable.humanlice,
                "lICE(Target temperature 122º-131ºF)\n" + "\n" +
                        "Lice are tiny, wingless, parasitic insects that feed on human blood. Lice are easily spread — especially by schoolchildren — through close personal contact and by sharing belongings.  People can have good personal hygiene and still get lice. Unless treated properly, this condition can become a recurring problem.\n" +
                        "\n" +
                        "THERE ARE THREE TYPE OF LICE:\n" +
                        "    HEAD LICE. These lice are found on your scalp. They’re easiest to see at the nape of the neck and over the ears.\n" + "\n" +
                        "    BODY LICE. These lice live in clothing and on bedding and move onto your skin to feed. Body lice most often affect people who aren’t able to bathe or launder clothing regularly, such as homeless individuals.\n" + "\n" +
                        "    PUBLIC LICE. Commonly called crabs, these lice occur on the skin and hair of the pubic area and, less frequently, on coarse body hair, such as chest hair, eyebrows or eyelashes.\n" +
                        "\n" +
                        "ITEMS FOR TREATMENT\n" +
                        "    Clothing and Bedding:  Treat clothing, bed linens, and other items that an infested person wore or used during the 2 days before beginning personal treatment.\n" + "\n" +
                        "    Personal Care Items:  Heat treat items that are in contact with hair such as combs, brushes, hair ribbons, barrettes and towels.  \n" + "\n" +
                        "    Heat Sensitive Items:  Any items that cannot be treated should sealed in a plastic bag and stored for 2 weeks (per CDC guidelines)\n" +
                        "\n"
            )
        )
        bugAbout.add(
            BugInfoModel(
                R.drawable.cloth_moth,
                "CLOTH MOTHS(Target temperature 125ºF)\n" + "\n" +
                        "Identification and Diet-  These moths feed exclusively on animal fibers such as wool, silk, felt, and leather.  These moths can live up to two years and start as creamy white caterpillars.\n" + "\n" +
                        "Treating Items-  Treating any natural fibers blankets or clothing.\n" +
                        "\n"
            )
        )
        bugAbout.add(
            BugInfoModel(
                R.drawable.pantry_moths,
                "PANTRY MOTHS(Target temperature 125ºF)\n" + "\n" +
                        "PANTRY OR INDIAN MOTHS\n" +
                        "Identification: These moths and their larvae can invade cupboard with grains and cereals.  It is also common for them to infest bird seed.\n" + "\n" +
                        "Inspect Food:  Food for human consumption infested with moths or larvae should be discarded.  Sealed containers may be salvaged\n" + "\n" +
                        "Birdseed and Other: Large bags of feed should be transferred to plastic storage containers and spread no deeper than 6 inches during 4-hour treatment.\n"
            )
        )
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        c = context
    }

    companion object {
        lateinit var bedBugAdapter: BedBugAdapter
        private lateinit var bugInfoAdapter: BugInfoAdapter

        fun newInstance() =
            BugInfoFragment()
    }


/* override fun bugInterface(bugInfoImg: Int, bugInfoDes: String) {
binding.bugImage.setImageResource(bugInfoImg)
binding.bugDes.text = bugInfoDes
}*/


}