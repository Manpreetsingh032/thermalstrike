package `in`.his.thermalstrike.fragment

import `in`.his.thermalstrike.BottomNavigation
import `in`.his.thermalstrike.HandleSharedPreference
import `in`.his.thermalstrike.R
import `in`.his.thermalstrike.adapter.OnClickDeviceList
import `in`.his.thermalstrike.databinding.FragmentTempMonitorBinding
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.fragment_temp_monitor.*
import kotlinx.android.synthetic.main.setting_up_device_item6.*
import kotlinx.android.synthetic.main.tempo_utility_item2_8.view.*
import kotlinx.android.synthetic.main.tempo_utility_item7.view.*


class TempMonitorFragment : Fragment() {
    private var fragmentTempMonitorBinding: FragmentTempMonitorBinding? = null
    private val binding get() = fragmentTempMonitorBinding!!
    lateinit var onClickDeviceList: OnClickDeviceList
    private lateinit var startNewDialog: Dialog
    private lateinit var countDownTimer: CountDownTimer
    var startInt = 0
    var endInt = 100
    lateinit var c: Context
    private var newInput = ""
    var value5: String? = null
    lateinit var handleSharedPreference: HandleSharedPreference


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        fragmentTempMonitorBinding =
            FragmentTempMonitorBinding.inflate(layoutInflater, container, false)

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleSharedPreference = HandleSharedPreference(c)

        val value1 = arguments?.getString("name")
        val value2 = arguments?.getString("strength")
        val value3 = arguments?.getString("battery")
        val value4 = arguments?.getString("temperature")
        value5 = arguments?.getString("address")
        Log.d("TAG", "onViewCreated:$value1")
        binding.deviceAddress.text = value1
        binding.strength.text = value2
        binding.battery.text = value3
        binding.temperature.text = value4


        startNewDialog = Dialog(requireContext())
        with(startNewDialog) {
            requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
            setContentView(R.layout.setting_up_device_item6)
            setCancelable(true)
            setCanceledOnTouchOutside(true)
            setting_up_dev_cancel.setOnClickListener {
                startNewDialog.dismiss()
            }
        }

        buttonClick()
        chartData()

    }

    private fun buttonClick() {

        binding.startNew.setOnClickListener {
            startNewDialog.show()

            requireActivity().runOnUiThread() {
                loggingInterval()
                startNewDialog.done_text1.text = "Done"
                alarmOneSet()
                startNewDialog.temp_text.text = newInput
                clearAlarms()
                alarmsOnOrOff()
                alarmInfo()
                clearData()
                startNewDialog.done_text2.text = "Done"
            }


        }

        binding.viewProcess.setOnClickListener {

            val inflater = layoutInflater
            val alertLayout: View = inflater.inflate(R.layout.tempo_utility_item7, null)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                alertLayout.tempo_utility_progress_bar.min = 0
            }
            alertLayout.tempo_utility_progress_bar.progress = 0
            alertLayout.tempo_utility_progress_bar.max = 100
            countDownTimer = object : CountDownTimer(100000, 1000) {
                override fun onTick(p0: Long) {
                    startInt += 1;
                    alertLayout.tempo_utility_progress_bar.progress = startInt;

                    alertLayout.tu_percentage1.text = "" + (startInt * 100) / endInt + "%";
                    alertLayout.tu_loading1.text = "$startInt/$endInt"
                }

                override fun onFinish() {

                }
            }.start()


            val builder: AlertDialog.Builder = AlertDialog.Builder(activity).setView(alertLayout)

            val dialog: AlertDialog = builder.create()

            // Finally, display the alert dialog

            // Finally, display the alert dialog
            dialog.show()

            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
            alertLayout.view_process_cancel.setOnClickListener {
                dialog.dismiss()
                alertLayout.tempo_utility_progress_bar.progress = 0
                alertLayout.tu_percentage1.text = "0"
                alertLayout.tu_loading1.text = "0/100"
                startInt = 0
                countDownTimer.cancel()
            }

        }
        binding.stopRec.setOnClickListener {
            val inflater = layoutInflater
            val alertLayout: View = inflater.inflate(R.layout.tempo_utility_item2_8, null)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                alertLayout.tempo_utility_progress_bar_a.min = 0
            }
            alertLayout.tempo_utility_progress_bar_a.progress = 0
            alertLayout.tempo_utility_progress_bar_a.max = 100
            countDownTimer = object : CountDownTimer(100000, 1000) {
                override fun onTick(p0: Long) {
                    startInt += 1;
                    alertLayout.tempo_utility_progress_bar_a.progress = startInt;

                    alertLayout.tu_percentage.text = "" + (startInt * 100) / endInt + "%";
                    alertLayout.tu_loading.text = "$startInt/$endInt"
                }

                override fun onFinish() {

                }
            }.start()


            val builder: AlertDialog.Builder = AlertDialog.Builder(activity).setView(alertLayout)

            val dialog: AlertDialog = builder.create()

            // Finally, display the alert dialog

            // Finally, display the alert dialog
            dialog.show()

            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
            alertLayout.stop_rec_cancel.setOnClickListener {
                dialog.dismiss()
                alertLayout.tempo_utility_progress_bar_a.progress = 0
                alertLayout.tu_percentage.text = "0"
                alertLayout.tu_loading.text = "0/100"
                startInt = 0
                countDownTimer.cancel()
            }
        }
    }


    private fun alarmOneSet() {

        val commandToReturn = "*alarm1t>"
        newInput = handleSharedPreference.getTemp().toString()
        returnCommandsToMain(commandToReturn + newInput)
        Log.d("TAG", "alarmOneSet:$newInput ")
    }


    private fun loggingInterval() {
        returnCommandsToMain("*lint60")

    }

    private fun alarmInfo() {
        returnCommandsToMain("*alrmi")
    }

    private fun alarmsOnOrOff() {
        val commandToReturn = "*alrm1"
        returnCommandsToMain(commandToReturn)
    }


    private fun clearData() {
        returnCommandsToMain("*clr")
    }

    private fun clearAlarms() {
        returnCommandsToMain("*alrmcl")
    }

    private fun returnCommandsToMain(command1: String) {

        abc = BottomNavigation.handleClick
        abc.goThere(command1, value5)
        /*  val b = Bundle()
          b.putString("Command", command)
          val result = Intent()
          result.putExtras(b)
          requireActivity().setResult(Activity.RESULT_OK, result)
          requireActivity().finish()*/
    }

    private fun chartData() {
        val entries = ArrayList<Entry>()

//Part2
        entries.add(Entry(1f, 60f))
        entries.add(Entry(2f, 70f))
        entries.add(Entry(3f, 70f))
        entries.add(Entry(4f, 80f))
        entries.add(Entry(5f, 86f))
//Part3
        val vl = LineDataSet(entries, "")

//Part4
        vl.setDrawValues(false)
        vl.setDrawFilled(false)
        vl.lineWidth = 3f
        vl.fillColor = R.color.secHover
        vl.fillAlpha = R.color.purple_200

//Part5
        process_chart.xAxis.labelRotationAngle = 0f

//Part6
        process_chart.data = LineData(vl)

//Part7
        process_chart.axisRight.isEnabled = false
        var j = 1
        process_chart.xAxis.axisMaximum = j + 0.1f
        /* val leftAxis = process_chart.axisLeft
          leftAxis.axisMinimum=10f
         leftAxis.axisMaximum=200f*/

//Part8
        process_chart.setTouchEnabled(true)
        process_chart.setPinchZoom(true)

//Part9
        process_chart.description.text = ""
//        process_chart.setNoDataText("No forex yet!")

//Part10
//    lineChart.animateX(1800, Easing.EaseInExpo)

//Part11
/*    val markerView = CustomMarker(requireContext(), R.id.process_chart)

    lineChart.marker = markerView*/

    }


    override fun onDestroyView() {
        super.onDestroyView()
        fragmentTempMonitorBinding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        c = context
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            TempMonitorFragment()

        lateinit var abc: ABC
    }
}

interface ABC {
    fun goThere(command: String, value5: String?)
}
