package `in`.his.thermalstrike.fragment

import `in`.his.thermalstrike.HandleSharedPreference
import `in`.his.thermalstrike.ParameterRoomDatabase
import `in`.his.thermalstrike.adapter.DeviceListAdapter
import `in`.his.thermalstrike.adapter.OnClickDeviceList
import `in`.his.thermalstrike.ble.ScanRecordParser
import `in`.his.thermalstrike.ble.Utility
import `in`.his.thermalstrike.database.NameTempTable
import `in`.his.thermalstrike.databinding.FragmentDeviceListBinding
import `in`.his.thermalstrike.device.BMDevice
import `in`.his.thermalstrike.device.BMDeviceMap
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Context.BLUETOOTH_SERVICE
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import java.io.UnsupportedEncodingException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.thread

class DeviceListFragment : Fragment() {
    private var fragmentDeviceListBinding: FragmentDeviceListBinding? = null
    private val binding get() = fragmentDeviceListBinding!!
    private lateinit var mBluetoothAdapter: BluetoothAdapter
    private lateinit var deviceList: ArrayList<BluetoothDevice>
    private lateinit var handler: Handler
    private val scanPeriod: Long = 40000 //scanning for 30 seconds
    private var mScanning = false
    private lateinit var bleScanner: BluetoothLeScanner
    private lateinit var c: Context
    private lateinit var db: ParameterRoomDatabase
//    private var list: ArrayList<DataModel> = ArrayList()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentDeviceListBinding = FragmentDeviceListBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleSharedPreference = HandleSharedPreference(c)
        handler = Handler(Looper.getMainLooper())
        db = ParameterRoomDatabase.getDatabase(c)

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        val bluetoothManager =
            requireContext().getSystemService(BLUETOOTH_SERVICE) as BluetoothManager
        mBluetoothAdapter = bluetoothManager.adapter
        bleScanner = mBluetoothAdapter.bluetoothLeScanner
        Log.d(TAG, "onCreate: $bleScanner")
        populateList()

    }

    private fun populateList() {
        /* Initialize device list container*/
        Log.d(TAG, "populateList")
        deviceList = ArrayList()
        deviceListAdapter = DeviceListAdapter()
        val linearLayoutManager =
            LinearLayoutManager(this.requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.deviceListRv.layoutManager = linearLayoutManager
        binding.deviceListRv.adapter = deviceListAdapter

        deviceListAdapter.onClickDeviceList = activity as OnClickDeviceList

        scanLeDevice(true)
    }

    private fun scanLeDevice(enable: Boolean) {
        if (enable) {
            // Stops scanning after a pre-defined scan period.
            handler.postDelayed({
                mScanning = false
                mBluetoothAdapter.bluetoothLeScanner.stopScan(object : ScanCallback() {
                    override fun onScanResult(callbackType: Int, result: ScanResult) {
                        Log.d(
                            TAG,
                            "onBatchScanResults: $result"
                        )
                        super.onScanResult(callbackType, result)
                    }

                    override fun onBatchScanResults(results: List<ScanResult>) {
                        Log.d(
                            TAG,
                            "onBatchScanResults: $results"
                        )
                        super.onBatchScanResults(results)
                    }

                    override fun onScanFailed(errorCode: Int) {
                        super.onScanFailed(errorCode)
                    }
                })
                //                    mBluetoothAdapter.stopLeScan(mLeScanCallback);

            }, scanPeriod)
            mScanning = true
            mBluetoothAdapter.bluetoothLeScanner
                .startScan(object : ScanCallback() {
                    override fun onScanResult(callbackType: Int, result: ScanResult) {
                        Log.d(
                            TAG,
                            "onBatchScanResults: $result"
                        )
                        addDevice(result.device, result.rssi, result.scanRecord!!.bytes)
                        super.onScanResult(callbackType, result)
                    }

                    override fun onBatchScanResults(results: List<ScanResult>) {
                        Log.d(
                            TAG,
                            "onBatchScanResults: " + results.size
                        )
                        super.onBatchScanResults(results)
                    }

                    override fun onScanFailed(errorCode: Int) {
                        Log.d(
                            TAG,
                            "onScanFailed: $errorCode"
                        )
                        super.onScanFailed(errorCode)
                    }
                })

        } else {
            mScanning = false
            //            mBluetoothAdapter.stopLeScan(mLeScanCallback);

        }
    }

    private fun addDevice(device: BluetoothDevice, rssi: Int, scanRecord: ByteArray) {
        var deviceFound = false
        Log.d(TAG, "addDevice: $device")
        for (listDev in deviceList) {
            if (listDev.address == device.address) {
                deviceFound = true
                break
            }
        }
        //Log.d("ScanRecordParser", "Scan Record data: 0x" + Utility.bytesAsHexString(scanRecord));
        var parser: ScanRecordParser? = null
        try {
            parser = ScanRecordParser(scanRecord)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }


        // Check if Blue Maestro device
        if (!BMDevice.isBMDevice(parser!!.manufacturerData)) return
        // Parse Blue Maestro device (future implementation required)
        var name: String?
        val scannedName = parser.scannedName
        Log.d(
            TAG,
            "In Device List activity and device is : " + device.address + " and scanned name is : " + scannedName + " and BT name is : " + device.name
        )
        name = if (scannedName !== "Unknown" && scannedName != null) {
            scannedName
        } else {
            device.name
        }
        if (name == null) {
            SystemClock.sleep(1000)
            name = device.name
        }
        var id = parser.manufacturerData[3]

        // Set the Tempo Disc T's id to 0x01, as this needs implementing
        if ("Tempo Disc T" == device.name) id = 0x01
        Log.d(TAG, "Device name: " + device.name)

        // Get Blue Maestro device, or create it if new device
        var bmDevice = BMDeviceMap.INSTANCE.getBMDevice(device.address)
        if (bmDevice == null || id != bmDevice.version) {
            bmDevice = BMDeviceMap.INSTANCE.createBMDevice(id, device)
        } else bmDevice.updateWithInfo(device)


        val myFormat = "yyyy-MM-dd"
        var timestamp= SimpleDateFormat(myFormat, Locale.getDefault())
        Thread {
            db.nameTempTableDao().insert(NameTempTable(0, name, timestamp.toString()))
        }.start()

        try {
            bmDevice!!.updateWithData(rssi, name, parser.manufacturerData, parser.scanResponseData)
        } catch (e: ArrayIndexOutOfBoundsException) {
            // Debugging, as some devices are still being worked on
            Log.e(
                "BMDeviceMap", "Exception: Device " + device.address +
                        "(" + device.name + ") is still in development."
            )
        }
        if (!deviceFound) {
            deviceList.add(device)
            Log.d(TAG, "Device: " + device.address)
        }
        deviceListAdapter.notifyDataSetChanged()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentDeviceListBinding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        c = context
    }
  /*  private fun storeNameTemp() {
        val myFormat = "yyyy-MM-dd"
        var timestamp= SimpleDateFormat(myFormat, Locale.getDefault())
        thread {
            val a = db.nameTempTableDao()
                .insert(
                    NameTempTable(
                        0,
                        deviceList[0].name,
                        timestamp.toString(),
                        deviceList[0].t

        }
    }*/

    companion object {
        lateinit var deviceListAdapter: DeviceListAdapter
        lateinit var handleSharedPreference: HandleSharedPreference


        fun newInstance() =
            DeviceListFragment()
    }
}