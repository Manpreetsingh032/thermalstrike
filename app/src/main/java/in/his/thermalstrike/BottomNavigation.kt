package `in`.his.thermalstrike

import `in`.his.thermalstrike.adapter.OnClickDeviceList
import `in`.his.thermalstrike.ble.Utility
import `in`.his.thermalstrike.databinding.BottomNavigationBinding
import `in`.his.thermalstrike.device.BMDeviceMap
import `in`.his.thermalstrike.fragment.*
import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.text.DateFormat
import java.util.*

class BottomNavigation : AppCompatActivity(), OnClickDeviceList {
    private lateinit var binding: BottomNavigationBinding
    private var mService: UartService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bottom_navigation)
        binding = BottomNavigationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container_1, DeviceListFragment.newInstance()).commit()

        binding.bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_settings -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container_1, SettingsFragment.newInstance(), "setting")
                        .commit()
                    binding.llRefresh.visibility = INVISIBLE
                    binding.appTitle.text = getString(R.string.please_choose_pest_or_custom_setting)

                }
                R.id.nav_bug_id -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container_1, BugInfoFragment.newInstance(), "bug").commit()
                    binding.llRefresh.visibility = INVISIBLE
                    binding.appTitle.text = getString(R.string.bug_identification)

                }
                R.id.nav_device -> {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container_1, DeviceListFragment.newInstance(), "home")
                        .commit()
                    binding.llRefresh.visibility = VISIBLE
                    binding.appTitle.text = getString(R.string.select_device)
                }
            }
            true
        }

        serviceInit()

        handleClick = object : ABC {
            override fun goThere(command: String, value5: String?) {
                Log.d("TAG", "goThere: $command")

                val qw: Boolean = mService!!.connect(value5)
                Log.d("TAG", "connection: $qw")
                commandMode = true
                commandSuccess = false

               this@BottomNavigation.command=command
                processCommand()
            }
        }
    }


    private fun serviceInit() {
        val bindIntent = Intent(this, UartService::class.java)
        bindService(bindIntent, mServiceConnection, BIND_AUTO_CREATE)
        makeGattUpdateIntentFilter().let {
            LocalBroadcastManager.getInstance(this)
                .registerReceiver(uartStatusChangeReceiver,
                    makeGattUpdateIntentFilter())
        }
    }


    /************************** UART STATUS CHANGE  */ // UART service connected/disconnected
    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, rawBinder: IBinder) {
            mService = (rawBinder as UartService.LocalBinder).service
            Log.d("TAG", "onServiceConnected mService= $mService")
            if (!mService!!.initialize()) {
                Log.e("TAG", "Unable to initialize Bluetooth")
                finish()
            }
        }

        override fun onServiceDisconnected(classname: ComponentName) {
            Log.d("TAG", "onService Disconnect")
            if (mService != null) mService!!.disconnect()
            mService = null
        }
    }

    //This is device details screen, with three buttons.  Download, graph and settings.
    override fun onDestroy() {
        super.onDestroy()
        Log.d("TAG", "onDestroy()")
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(uartStatusChangeReceiver)
        } catch (ignore: Exception) {
            Log.e("TAG", ignore.toString())
        }
        try {
            unbindService(mServiceConnection)
            if (mService != null) {
                mService!!.stopSelf()
                mService = null
            }
            BMDeviceMap.INSTANCE.clear()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    // Main UART broadcast receiver
    private val uartStatusChangeReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("TAG", "onReceive:$intent ")
            Log.d("TAG", intent.getStringExtra("Key").toString())
            when (intent.action) {
                UartService.ACTION_GATT_CONNECTED -> {
                    onGattConnected()
                }
                UartService.ACTION_GATT_DISCONNECTED -> {
                    onGattDisconnected()
                }
                UartService.ACTION_GATT_SERVICES_DISCOVERED -> {
                    onGattServicesDiscovered()
                }
                UartService.ACTION_DATA_AVAILABLE -> {
                    intent.getByteArrayExtra(UartService.EXTRA_DATA)?.let { onDataAvailable(it) }
                }
                UartService.DEVICE_DOES_NOT_SUPPORT_UART -> {
                    onDeviceDoesNotSupportUART()
                }
                UartService.UART_TX_ENABLED -> {
                    onUARTTXEnabled()
                }
                else -> {
                }
            }
        }
    }

    @Volatile
    private var unstableBluetooth = false
    private val REQUEST_SELECT_DEVICE = 1
    private val REQUEST_ENABLE_BT = 2
    private val SELECT_STORED_DATA = 3
    private val REQUEST_COMMAND = 5
    private val UART_PROFILE_READY = 10

    private val uartProfileConnected = 20
    private val uartProfileDisconnected = 21
    private var mState: Int = uartProfileDisconnected

    //For commands to device
    private var commandMode = false //Whether trying to pass a command

    private var command //The actual command to pass
            : String? = null
    private var commandSuccess = false //Whether a command was actually passed or not

    private var commandResponse //The device's response to the command
            : String? = null
    private var automaticDownload = false

    private fun onGattConnected() {
        runOnUiThread {
            val currentDateTimeString =
                DateFormat.getTimeInstance().format(Date())
            Log.d("TAG", "UART_CONNECT_MSG")
            unstableBluetooth = true
            mState = uartProfileConnected
            if (!automaticDownload && !commandMode) {
//               messageListView.smoothScrollToPosition(listAdapter.getCount() - 1)
            }
        }
    }

    private fun onGattDisconnected() {
        runOnUiThread {
            val currentDateTimeString = DateFormat.getTimeInstance().format(Date())
            Log.d("TAG", "UART_DISCONNECT_MSG")
            if (!automaticDownload && !commandMode) {
                /*val deviceName = findViewById<View>(R.id.deviceName) as TextView
                if (deviceName != null) deviceName.text = "Not Connected"
                listAdapter.add("Disconnected from: " + mDevice.getName() + " (" + mDevice.getAddress() + ")")
                listAdapter.notifyDataSetChanged()*/
                mState = uartProfileDisconnected
//                messageListView.setSelection(listAdapter.getCount() - 1)
            }
            mService!!.close()
            automaticDownload = false
            commandMode = false
        }
    }

    private fun onGattServicesDiscovered() {
        unstableBluetooth = false
        Log.d("TAG", "ENABLETXNOTIFICATION")
        if (automaticDownload) {
            download()
        }
        if (commandMode) {
            mService?.enableTXNotification() //Is this right?
            processCommand()
        }
    }

    private fun onUARTTXEnabled() {
        if (automaticDownload) {
            try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            download()
        }
    }

    private fun onDataAvailable(txValue: ByteArray) {
        Log.d("TAG", "DATAAVAILABLE")
        runOnUiThread {
            try {
                val text = String(txValue, Charset.forName("UTF-8")).trim { it <= ' ' }
                Log.d("TAG", "Received message is : $text")
                if (commandMode) {
                    commandResponse = text
                    commandSuccess = true
                    /*showAlert("Response from device is : \n\n$commandResponse")
                    mBMDevice.updateChart(lineChart, text)*/
                } else {
                    val currentDateTimeString = DateFormat.getTimeInstance().format(Date())
                    /*  listAdapter.add(text)
                      listAdapter.notifyDataSetChanged()
                      if (messageListView.getVisibility() == View.VISIBLE) {
                          messageListView.smoothScrollToPosition(listAdapter.getCount() - 1)
                      } else {
                          messageListView.setSelection(listAdapter.getCount() - 1)
                      }
                      if (mBMDevice == null) return@Runnable
                      mBMDevice.updateChart(lineChart, text)
                      if (mBMDatabase == null) return@Runnable*/
                    //mBMDatabase.addData(BMDatabase.TIMESTAMP_NOW(), text);
                }
            } catch (e: Exception) {
                Log.e("TAG", e.toString())
            }
        }
    }

    private fun onDeviceDoesNotSupportUART() {
//        showMessage("Device doesn't support UART. Disconnecting")
        mService?.disconnect()
    }

    private fun makeGattUpdateIntentFilter(): IntentFilter {
        val intentFilter = IntentFilter()
        intentFilter.addAction(UartService.ACTION_GATT_CONNECTED)
        intentFilter.addAction(UartService.ACTION_GATT_DISCONNECTED)
        intentFilter.addAction(UartService.ACTION_GATT_SERVICES_DISCOVERED)
        intentFilter.addAction(UartService.ACTION_DATA_AVAILABLE)
        intentFilter.addAction(UartService.DEVICE_DOES_NOT_SUPPORT_UART)
        intentFilter.addAction(UartService.UART_TX_ENABLED)
        return intentFilter
    }

    private fun download() {
        /*if (mBMDevice == null || mBMDatabase == null || mService == null) {
            Log.d(
                "TAG",
                "In download method and there is no device, database or service"
            )
            dialogStart.dismiss()
            val message = "Problem downloaded logs from " + mBMDatabase.getAddress()
            showAlert(message)
            return
        }
        if (!mBMDevice.isDownloadReady()) {
            // Device not ready for downloading - display alert message
            dialogStart.dismiss()
            showAlert("Unable to download from device. Please change your current mode.")
            val message = "Problem downloaded logs from " + mBMDatabase.getAddress()
            showAlert(message)
            return
        }*/

        // Downloader
//        val downloader = BMDownloader(mBMDatabase, progressIndicatorDownload)

        //The old code starts below and was immediately under final BMDownloader downloader....


        // Switch out BroadcastReceiver
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(uartStatusChangeReceiver)
        /* LocalBroadcastManager.getInstance(this).registerReceiver(
                 downloader,
             makeGattUpdateIntentFilter()!!
         )*/
        mService!!.enableTXNotification()
        Thread {
//            var state: DownloadState = DownloadState.FAILURE_NOT_DEFINED
            try {
//                state = mBMDatabase.downloadData(mService, downloader, progressIndicatorDownload)
            } catch (e: UnsupportedEncodingException) {
                Log.d("TAG", "Download method in main and in exception")
                e.printStackTrace()
            }
            // Switch back in BroadcastReceivers
//            LocalBroadcastManager.getInstance(this).unregisterReceiver(downloader)
            LocalBroadcastManager.getInstance(this).registerReceiver(
                uartStatusChangeReceiver,
                makeGattUpdateIntentFilter()
            )
//            progressIndicatorDownload.dismiss()
//            Log.d("TAG", "Download state: $state")
            /*     val message =
                     if (state === DownloadState.SUCCESS) "Successfully downloaded logs from " + mBMDatabase.getAddress() else "Failed to download logs from " + mBMDatabase.getAddress()

                 //onGattDisconnected();
                 showAlert(message)*/
        }.start()
    }

    private fun processCommand() {

        try {
            Log.d("TAG", "About to send command to device and it is : $command")
            Utility.sendCommand(mService, command)
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }


    override fun onClickListItems(
        name: String?,
        strength: String?,
        battery: String?,
        temperature: String?,
        address:String?
    ) {

        val ldf = TempMonitorFragment()
        val args = Bundle()
        args.putString("name", name)
        args.putString("address", address)
        args.putString("strength", strength)
        args.putString("battery", battery)
        args.putString("temperature", temperature)
        ldf.arguments = args


        Log.d("TAG", "onClickListItems:$address ")
        supportFragmentManager.beginTransaction()
            .replace(R.id.container_1, ldf)
            .commit()

        binding.llRefresh.visibility = INVISIBLE
        binding.appTitle.text = getString(R.string.temperature_monitor)
    }



    companion object {
        lateinit var handleClick: ABC
    }

}