package `in`.his.thermalstrike

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("CommitPrefEdits")
class HandleSharedPreference(private val c: Context) {

    init {
        sharedPreferences =
            c.applicationContext?.getSharedPreferences(preferences, Context.MODE_PRIVATE)
    }


    fun insertSku(skuList: Set<String>) {
        editor = sharedPreferences?.edit()!!
        editor.remove(skuPreference)
        editor.putStringSet(skuPreference, skuList)
        editor.apply()
        editor.commit()
    }

    fun skuList() = run {
        sharedPreferences?.getStringSet(skuPreference, emptySet())
    }

    fun insertLineNos(lineList: Set<String>) {
        editor = sharedPreferences?.edit()!!
        editor.remove(linePreference)
        editor.putStringSet(linePreference, lineList)
        editor.apply()
        editor.commit()
    }

    fun lineList() = run {
        sharedPreferences?.getStringSet(linePreference, emptySet())
    }

    fun getUnit(): String? {
        return sharedPreferences?.getString(tempUnitPreference, "Fahrenheit")
    }

    fun getTemp(): String? {
        return sharedPreferences?.getString(tempPreference, "120")
    }

    fun getTime(): String? {
        return sharedPreferences?.getString(timePreference, "30")
    }

    fun getPredictiveValueSwitch(): Boolean? {
        return sharedPreferences?.getBoolean(predictivePreference,false)
    }

    fun getAlarmSwitch(): Boolean? {
        return sharedPreferences?.getBoolean(alarmPreference, false)
    }

    fun getEmailSwitch(): Boolean? {
        return sharedPreferences?.getBoolean(emailPreference, false )
    }

    fun saveUnit(user: String) {
        editor = sharedPreferences?.edit()!!
        editor.putString(tempUnitPreference, user)
        editor.apply()
        editor.commit()
    }

    fun saveTemp(temperature: String) {
        editor = sharedPreferences?.edit()!!
        editor.putString(tempPreference, temperature)
        editor.apply()
        editor.commit()
    }

    fun saveTime(timee: String) {
        editor = sharedPreferences?.edit()!!
        editor.putString(timePreference, timee)
        editor.apply()
        editor.commit()
    }

    fun savePredictive(boolean: Boolean) {
        editor = sharedPreferences?.edit()!!
        editor.putBoolean(predictivePreference,boolean)
        editor.apply()
        editor.commit()
    }

    fun saveAlarm(boolean: Boolean) {
        editor = sharedPreferences?.edit()!!
        editor.putBoolean(alarmPreference, boolean)
        editor.apply()
        editor.commit()
    }

    fun saveEmail(boolean: Boolean) {
        editor = sharedPreferences?.edit()!!
        editor.putBoolean(emailPreference,boolean)
        editor.apply()
        editor.commit()
    }


    companion object {
        private const val preferences = "Preference"
        private const val skuPreference = "skuPreference"
        private const val linePreference = "LinePreference"
        private const val tempUnitPreference = "TempUnitPreference"
        private const val tempPreference = "TempPreference"
        private const val timePreference = "TimePreference"
        private const val predictivePreference = "PredictivePreference"
        private const val alarmPreference = "AlarmPreference"
        private const val emailPreference = "EmailPreference"
        private lateinit var editor: SharedPreferences.Editor
        var sharedPreferences: SharedPreferences? = null
    }
}