package `in`.his.thermalstrike.adapter

import `in`.his.thermalstrike.databinding.BedbugsItemBinding
import `in`.his.thermalstrike.fragment.BugInfoFragment
import `in`.his.thermalstrike.fragment.DeviceListFragment
import `in`.his.thermalstrike.models.BedBugModel
import `in`.his.thermalstrike.models.BugInfoModel
import `in`.his.thermalstrike.models.DataModel
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class BedBugAdapter(val context: Context) : RecyclerView.Adapter<BedBugAdapter.MyViewHolder>() {
    private var bugList = ArrayList<BedBugModel>()
    private var bugInfoList = ArrayList<BugInfoModel>()
//    lateinit var bugInterface: BugInterface

    inner class MyViewHolder(val bedbugsItemBinding: BedbugsItemBinding) :
        RecyclerView.ViewHolder(bedbugsItemBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            BedbugsItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        val currentBugList = bugInfoList[position]
        holder.bedbugsItemBinding.bugImage.setImageResource(bugList[position].bugImage)
        holder.bedbugsItemBinding.ttName.text = bugList[position].bugName
        holder.bedbugsItemBinding.aboutBug.text = bugList[position].bugAbout
        holder.bedbugsItemBinding.bottomDes.text = bugList[position].bugBottomDes
        /*holder.itemView.setOnClickListener {
            if (this::bugInterface.isInitialized) {
                bugInterface.bugInterface(position)
            }
        }*/
    }

    override fun getItemCount(): Int {
        return bugList.size
    }

    fun setList(bugs: ArrayList<BedBugModel>) {
        this.bugList = bugs
//        this.bugInfoList = bugsDes
//        this.bugInterface = bugInterface
        notifyDataSetChanged()
    }
    companion object {
        val a = object : BugChange {
            override fun bugData(
                listBug: ArrayList<BedBugModel>,
//                bugsDes: ArrayList<BugInfoModel>,
//                bugInterface: BugInterface
            ) {
                BugInfoFragment.bedBugAdapter.setList(listBug )
            }
        }
    }

}
interface BugChange {
    fun bugData(listBug: ArrayList<BedBugModel>)
}

/*
interface BugInterface {
    fun bugInterface(position: Int)
}*/
