package `in`.his.thermalstrike.adapter
import `in`.his.thermalstrike.databinding.BedbugsItemBinding
import `in`.his.thermalstrike.fragment.SettingsFragment
import `in`.his.thermalstrike.models.BedBugModel
import `in`.his.thermalstrike.models.TempTimeModel
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class SettingsBedBugAdapter(val context: Context): RecyclerView.Adapter<SettingsBedBugAdapter.MyViewHolder>() {
    private var bugList = ArrayList<BedBugModel>()
    private var bugTempTimeList = ArrayList<TempTimeModel>()
//    private lateinit var bugTempTime: TempTimeInterface

    inner class MyViewHolder( val bedbugsItemBinding: BedbugsItemBinding) :
        RecyclerView.ViewHolder(bedbugsItemBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            BedbugsItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentTT = bugTempTimeList[position]
        holder.bedbugsItemBinding.bugImage.setImageResource(bugList[position].bugImage)
        holder.bedbugsItemBinding.ttName.text = bugList[position].bugName
        holder.bedbugsItemBinding.aboutBug.text = bugList[position].bugAbout
        holder.bedbugsItemBinding.bottomDes.text = bugList[position].bugBottomDes
        /*holder.itemView.setOnClickListener {
            if (this::bugTempTime.isInitialized) {
                bugTempTime.bugTempTime(currentTT.bugTemp, currentTT.bugTime,position)
            }
        }*/

    }

    override fun getItemCount(): Int {
        return bugList.size
    }

    fun setList(
        bugs: ArrayList<BedBugModel>,
        tt:ArrayList<TempTimeModel>,
//        bugTempTimeInterface: TempTimeInterface
    ) {
        this.bugList = bugs
        this.bugTempTimeList=tt
//        this.bugTempTime=bugTempTimeInterface
        notifyDataSetChanged()
    }

    companion object {
        val a = object : BugInfoChange {
            override fun bugInfoData(
                listBug: ArrayList<BedBugModel>,
                bugsDes:ArrayList<TempTimeModel>,
//                tempTimeInterface: TempTimeInterface
            ) {
                SettingsFragment.settingsBedBugAdapter.setList(listBug,bugsDes)
            }
        }
    }

}

interface BugInfoChange {
    fun bugInfoData(listBug: ArrayList<BedBugModel>,bugsDes: ArrayList<TempTimeModel>)
}

interface TempTimeInterface {
    fun bugTempTime(bugTemp: String, bugTime: String,position: Int)
}

