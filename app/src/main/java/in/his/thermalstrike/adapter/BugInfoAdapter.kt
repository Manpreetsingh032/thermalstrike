package `in`.his.thermalstrike.adapter

import `in`.his.thermalstrike.databinding.BugInfoDesBinding
import `in`.his.thermalstrike.models.BugInfoModel
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class BugInfoAdapter(val context: Context) : RecyclerView.Adapter<BugInfoAdapter.MyViewHolder>() {
    private var bugInfoList = ArrayList<BugInfoModel>()

    inner class MyViewHolder(val bugInfoItemBinding: BugInfoDesBinding) :
        RecyclerView.ViewHolder(bugInfoItemBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            BugInfoDesBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bugInfoItemBinding.bugImage.setImageResource(bugInfoList[position].bugInfoImg)
        holder.bugInfoItemBinding.bugDes.text = bugInfoList[position].bugInfoDes

    }

    override fun getItemCount(): Int {
        return bugInfoList.size
    }

    fun setList(bugs: ArrayList<BugInfoModel>) {
        this.bugInfoList = bugs
        notifyDataSetChanged()
    }

}