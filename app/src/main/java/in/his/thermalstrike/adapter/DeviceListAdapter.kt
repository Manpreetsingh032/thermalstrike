package `in`.his.thermalstrike.adapter

import `in`.his.thermalstrike.database.DeviceListTable
import `in`.his.thermalstrike.models.DataModel
import `in`.his.thermalstrike.databinding.DeviceListBinding
import `in`.his.thermalstrike.fragment.DeviceListFragment
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


class DeviceListAdapter : RecyclerView.Adapter<DeviceListAdapter.MyViewHolder>() {
    private var deviceList = ArrayList<DataModel>()
    lateinit var onClickDeviceList: OnClickDeviceList

    inner class MyViewHolder(val deviceListBinding: DeviceListBinding) :
        RecyclerView.ViewHolder(deviceListBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            DeviceListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        /*if (deviceList[position].devName == "abc"){

        }*/

        val currentDeviceList = deviceList[position]
        holder.deviceListBinding.deviceAddress.text = deviceList[position].devName
        holder.deviceListBinding.strength.text = deviceList[position].devStr
        holder.deviceListBinding.battery.text = deviceList[position].devBat
        holder.deviceListBinding.temperature.text = deviceList[position].devTemp
        holder.itemView.setOnClickListener {
            holder.deviceListBinding.parentView.setCardBackgroundColor(Color.GRAY)
            if (this::onClickDeviceList.isInitialized) {
                onClickDeviceList.onClickListItems(
                    currentDeviceList.devName,
                    currentDeviceList.devStr,
                    currentDeviceList.devBat,
                    currentDeviceList.devTemp,
                    currentDeviceList.devAdd
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return deviceList.size
    }

    fun setList(listDev: ArrayList<DataModel>) {
        this.deviceList = listDev
        notifyDataSetChanged()
    }


    companion object {
        val a = object : DataChange {
            override fun changeData(listDev: ArrayList<DataModel>) {
                DeviceListFragment.deviceListAdapter.setList(listDev)
            }
        }
    }
}

interface DataChange {
    fun changeData(listDev: ArrayList<DataModel>)
}

interface OnClickDeviceList {
    fun onClickListItems(
        name: String?,
        strength: String?,
        battery: String?,
        temperature: String?,
        address:String?
    )
}

/*
private var deviceList = ArrayList<DeviceListTable>()
lateinit var onClickDeviceList: OnClickDeviceList

inner class MyViewHolder(val deviceListBinding: DeviceListBinding) :
    RecyclerView.ViewHolder(deviceListBinding.root)

override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
    return MyViewHolder(
        DeviceListBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )
}

override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

    val currentDeviceList = deviceList[position]
    holder.deviceListBinding.deviceAddress.text = deviceList[position].address
    holder.deviceListBinding.strength.text = deviceList[position].strength
    holder.deviceListBinding.battery.text = deviceList[position].battery
    holder.deviceListBinding.temperature.text = deviceList[position].temperature
    holder.itemView.setOnClickListener {
        holder.deviceListBinding.parentView.setCardBackgroundColor(Color.GRAY)
        if (this::onClickDeviceList.isInitialized) {
            onClickDeviceList.onClickListItems(
                currentDeviceList.address,
                currentDeviceList.strength,
                currentDeviceList.battery,
                currentDeviceList.temperature
            )
        }
    }
}

override fun getItemCount(): Int {
    return deviceList.size
}

fun setList(listDev: ArrayList<DeviceListTable>) {
    this.deviceList = listDev
    notifyDataSetChanged()
}


companion object {
    val a = object : DataChange {
        override fun changeData(listDev: ArrayList<DeviceListTable>) {
            DeviceListFragment.deviceListAdapter.setList(listDev)
        }
    }
}
}

interface DataChange {
    fun changeData(listDev: ArrayList<DeviceListTable>)

}*/
