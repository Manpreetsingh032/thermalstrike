package `in`.his.thermalstrike

import `in`.his.thermalstrike.databinding.ActivityMainBinding
import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    lateinit var mBtAdapter: BluetoothAdapter
    private val REQUEST_ENABLE_BT = 2
    private val REQUEST_SELECT_DEVICE = 1
    private val requestEnableBt = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bleLocationPermission()
        startProcessClick()

    }

    private fun bleLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
                )
            }
        }
    }

    private fun startProcessClick() {
        binding.startProcess.setOnClickListener { onClickSelectDevice() }
    }

   /* override fun onResume() {
        super.onResume()
        mBtAdapter = BluetoothAdapter.getDefaultAdapter()
        if (!mBtAdapter.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != requestEnableBt && resultCode == RESULT_CANCELED) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(
                enableBtIntent,
                requestEnableBt
            )
        }
        super.onActivityResult(requestCode, resultCode, data)
    }*/
   override fun onResume() {
       super.onResume()
       mBtAdapter = BluetoothAdapter.getDefaultAdapter()
       if (!mBtAdapter.isEnabled) {
           val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
           resultLauncher.launch(enableIntent)
       }
   }
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val data: Intent? = result.data
            onResume()
        }
    }

    private fun onClickSelectDevice() {
        if (!mBtAdapter.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            resultLauncher.launch(enableIntent)
        } else {
            // Connect button pressed, open DeviceListActivity class, with popup windows that scan for devices
            val newIntent = Intent(this@MainActivity, BottomNavigation::class.java)
            resultLauncher.launch(newIntent)
        }
    }

}